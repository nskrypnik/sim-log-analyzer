import sys

from optparse import OptionParser
from models import *

parser = OptionParser()

parser.add_option('-s', dest="softlog")
parser.add_option('-k', dest='kernlog')
parser.add_option('-n', dest='sessionname')

(options, args) = parser.parse_args()

if options.sessionname is None:
    print "Pls, specify the session name"
    sys.exit(0)

soft_log = open(options.softlog, 'rb').read().split('\n')
#kern_log = open(options.kernlog, 'rb').read().split('\n')

#First find in kern log last bank session

session_log_start = 0

'''
for i in range(len(kern_log)):
    item = kern_log[i].split()
    if item and item[7] == '##simbank_start':
        session_log_start = i
'''

# Start new LogSession
session = Session()
logSession = LogSession(name=options.sessionname)
session.add(logSession)
session.commit()
session.close()

raw_data_from_driver = ''
card_sessions = {}
card_session_indeces = {}
# Start analyze soft log
for row in soft_log:
    #print 'current:', row
    #print 'Raw data cache: %s', raw_data_from_driver.__repr__()
    if row[0] == '#':
        'some command like send data or reset'
        op = row.split()[0]
        if op == '#reset_sim':
            #get card no here
            session = Session()
            card_no = int(row.split()[1])
            new_card_session = CardSession(card_id=card_no, session=logSession)
            session.add(new_card_session)
            session.commit()
            session.close()
            card_sessions[card_no] = new_card_session
            card_session_indeces[card_no] = 0

        if op == '#send_to_sim':
            pass

        continue
    if row[0] == "'":
        raw_data_from_driver = eval(row)
    else:
        # We get here handled data from soft
        #first check if it's valid data
        timestamp = int(row.split()[0])
        sim_no = int(row.split()[1])
        byte = int(row.split()[2], 16)
        state = int(row.split()[4])

        assert sim_no == ord(raw_data_from_driver[0])
        assert byte == ord(raw_data_from_driver[1]), "Byte: %X, raw_string: %s" % (byte, raw_data_from_driver)
        raw_data_from_driver = raw_data_from_driver[2:]
        session = Session()
        # If all right - create log message
        new_log_message = SoftCardLog(cardsession=card_sessions[sim_no],
                            cardsession_index=card_session_indeces[sim_no],
                            byte=byte,
                            handlerstate=state,
                            timestamp=timestamp
        )
        card_session_indeces[sim_no] += 1
        session.add(new_log_message)
        session.commit()
        session.close()



