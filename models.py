import  sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import *
from sqlalchemy.orm import relation

Base = declarative_base()
engine = create_engine('sqlite:///loganalyze.db')


class LogSession(Base):
    __tablename__ = 'logsession'
    id = Column(Integer(), primary_key=True)
    name = Column(String())

class Card(Base):
    __tablename__ = 'card'
    id = Column(Integer(), primary_key=True)


class CardSession(Base):
    __tablename__ = 'cardsession'
    id = Column(Integer(), primary_key=True)
    card_id = Column(Integer(), ForeignKey('card.id'))
    card = relation(Card, backref="soft_log_messages")
    session_id = Column(Integer(), ForeignKey('logsession.id'))
    session = relation(LogSession, backref="cardsessions")


class SoftCardLog(Base):
    __tablename__ = 'softcardlog'
    id = Column(Integer(), primary_key=True)
    cardsession_id = session_id = Column(Integer(), ForeignKey('cardsession.id'))
    cardsession_index = Column(Integer())
    byte = Column(Integer())
    timestamp = Column(Integer())
    handlerstate = Column(Integer())
    cardsession = relation(CardSession, backref="soft_log_messages")

class KernCardLog(Base):
    __tablename__ = 'kerncardlog'
    id = Column(Integer(), primary_key=True)
    cardsession_id = session_id = Column(Integer(), ForeignKey('cardsession.id'))
    cardsession_index = Column(Integer())
    byte = Column(Integer())
    timestamp = Column(Float())
    cardsession = relation(CardSession, backref="kern_log_messages")

Session = sessionmaker(bind=engine)
Base.metadata.create_all(engine)

# create all 200 cards

for i in range(200):
    session = Session()
    card = session.query(Card).get(i)
    if card is None:
        card = Card(id=i)
        session.add(card)
        session.commit()
        session.close()