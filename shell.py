from models import *

CURRENT_SIM = 0
CURRENT_SESSION = 0

def current_sim(sim_no=None):
    global CURRENT_SIM
    if sim_no:
        CURRENT_SIM = sim_no
    else: return CURRENT_SIM
    
def current_session(sess_id=None):
    global CURRENT_SESSION
    if sess_id:
        CURRENT_SESSION = sess_id
    else: return CURRENT_SESSION

def sessions_list():
    for sess in session.query(LogSession).all():
        print "%s %s" % (sess.id, sess.name)

def card_sessions_count():
    session = Session()
    return session.query(CardSession).filter(CardSession.card_id == CURRENT_SIM, CardSession.session_id == CURRENT_SESSION).count()
    
def get_card_session_log(sess_id):
    sessions = session.query(CardSession).filter(CardSession.card_id == CURRENT_SIM, CardSession.session_id == CURRENT_SESSION).all()
    try:
        card_sess = sessions[sess_id]
    except IndexError:
        print "No such card session"
        return
    logs = session.query(SoftCardLog).filter(SoftCardLog.cardsession_id == card_sess.id)
    for l in logs:
        print "%s %s %X %s" % (l.cardsession_index, l.timestamp, l.byte, l.handlerstate)


import ipdb; ipdb.set_trace()
